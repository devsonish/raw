﻿using System;

namespace NReality.MyTwitter.Exceptions
{
    public class AuthorisationException : Exception
    {
        public AuthorisationException()
        {

        }

        public AuthorisationException(string message)
            : base(message)
        {
        }
    }
}
