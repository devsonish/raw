﻿using System;

namespace NReality.MyTwitter.Exceptions
{
    internal class TweetSaveException : Exception
    {
        public TweetSaveException()
        {
        }

        public TweetSaveException(string message)
            : base(message)
        {
        }
    }
}