﻿using System;

namespace NReality.MyTwitter.Exceptions
{
    internal class TwitterAuthorisaionException : Exception
    {
        public TwitterAuthorisaionException()
        {
        }

        public TwitterAuthorisaionException(string message)
            : base(message)
        {
        }
    }
}