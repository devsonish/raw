﻿using System;
using System.Threading;
using NReality.MyTwitter.Artefacts.Security;
using NReality.MyTwitter.Helpers;

namespace NReality.MyTwitter.Cache.Security
{
    internal class UserCache : IDisposable
    {
        public static Users Instance
        {
            get
            {
                if (_instance == null)
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                        {
                            //Load default user for testing...
                            _instance = new Users();
                            _instance.Add(new User
                            {
                                Password = "P@ssword123",
                                UserId = 2,
                                UserName = "TheUser"
                            });
                        }
                    }

                return _instance;
            }

            set { _instance = value; }
        }

        void IDisposable.Dispose()
        {
            Dispose();
        }

        public static bool Refresh()
        {
            if (_instance == null)
                return false;

            var isRefreshed = false;

            // Wait until the lock is available and lock the queue.
            try
            {
                if (Monitor.TryEnter(_instance, GlobalSettings.Instance.ThreadEnterTimeout)) //wait for thead until ready with specified timeout...
                {
                    Dispose();
                    isRefreshed = true;
                }
            }
            finally
            {
                try
                {
                    Monitor.Exit(_instance);
                }
                catch (ArgumentNullException)
                {
                    //when the instance is null it will be refreshed when read.
                    isRefreshed = true;
                }
            }

            return isRefreshed;
        }

        public static void AddUser(User user)
        {
            if (!Instance.Contains(user))
                Instance.Add(user);
        }

        private static void Dispose()
        {
            GC.SuppressFinalize(_instance);
            GC.SuppressFinalize(_syncRoot);

            _instance = null;
            _syncRoot = new object();
        }

        #region private members...

        private static volatile Users _instance;
        private static object _syncRoot = new object();

        #endregion private members...
    }
}