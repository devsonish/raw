﻿using System;
using System.Threading;
using NReality.MyTwitter.Artefacts.Twitter;
using NReality.MyTwitter.Helpers;

namespace NReality.MyTwitter.Cache.Twitter
{
    public class SaveTweetCache : IDisposable
    {
        public static SaveTweets Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new SaveTweets();
                        }
                    }
                }

                return _instance;
            }

            set { _instance = value; }
        }

        void IDisposable.Dispose()
        {
            Dispose();
        }

        public static bool Refresh()
        {
            if (_instance == null)
                return false;

            var isRefreshed = false;

            // Wait until the lock is available and lock the queue.
            try
            {
                if (Monitor.TryEnter(_instance, GlobalSettings.Instance.ThreadEnterTimeout)) //wait for thead until ready with specified timeout...
                {
                    Dispose();
                    isRefreshed = true;
                }
            }
            finally
            {
                try
                {
                    Monitor.Exit(_instance);
                }
                catch (ArgumentNullException)
                {
                    //when the instance is null it will be refreshed when read.
                    isRefreshed = true;
                }
            }

            return isRefreshed;
        }

        private static void Dispose()
        {
            GC.SuppressFinalize(_instance);
            GC.SuppressFinalize(_syncRoot);

            _instance = null;
            _syncRoot = new object();
        }

        #region private members...

        private static volatile SaveTweets _instance;
        private static object _syncRoot = new object();

        #endregion private members...
    }
}