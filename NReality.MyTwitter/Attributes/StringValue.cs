﻿using System;

namespace NReality.MyTwitter.Attributes
{
    public class StringValueAttribute : Attribute
    {
        #region Properties...

        /// <summary>
        /// Holds the stringvalue for a value in an enum.
        /// </summary>
        public string StringValue { get; protected set; }

        #endregion Properties...

        #region Constructor...

        /// <summary>
        /// Constructor used to init a StringValue Attribute
        /// </summary>
        /// <param name="value"></param>
        public StringValueAttribute(string value)
        {
            StringValue = value;
        }

        #endregion Constructor...

        public static class StringEnum
        {
            public static string GetStringValue(Enum value)
            {
                // Get the type
                var type = value.GetType();

                // Get fieldinfo for this type
                var fieldInfo = type.GetField(value.ToString());

                // Get the stringvalue attributes
                var attribs = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];

                // Return the first if there was a match.
                if (attribs != null)
                {
                    return attribs.Length > 0 ? attribs[0].StringValue : null;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
