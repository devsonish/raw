﻿namespace NReality.MyTwitter.Helpers
{
    public enum ErrorType
    {
        UNKNOWN,
        SystemError,
        AdminOnly,
        Serialization_Faillure,
        Authorisation_AccountLockedOut,
        Authorisation_Denied,
        JSONFormat,
        SaveTweet_Failure
    }
}
