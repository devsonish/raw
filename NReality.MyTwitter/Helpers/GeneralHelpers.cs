﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Helpers;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NReality.MyTwitter.Helpers
{
    public static class GeneralHelper
    {
        #region Serialization

        public static String SerializeList<T>(List<T> data)
        {
            var sb = new StringBuilder();

            //Use reflection to just list all props
            var t = typeof(T);
            var props = t.GetProperties();

            sb.Append("<" + t.Name + "List>\r\n");

            foreach (var item in data)
            {
                sb.Append("\t<" + item.GetType().Name + " ");
                foreach (var prop in props)
                {
                    sb.Append(prop.Name + "=\"");

                    if (prop.GetValue(item, null) != null)
                        sb.Append(prop.GetValue(item, null));

                    sb.Append("\" ");
                }
                sb.Append("/>\r\n");
            }

            sb.Append("</" + t.Name + "List>");

            return sb.ToString();
        }

        public static List<T> DeserializeList<T>(String xml)
        {
            var retVal = new List<T>();
            T item;

            //Use reflection to just list all props
            var t = typeof(T);

            var fields = t.GetFields();

            var doc = new XmlDocument();
            doc.LoadXml(xml);
            var nodes = doc.GetElementsByTagName(t.Name);
            foreach (XmlNode node in nodes)
            {
                item = (T)Activator.CreateInstance(t);

                foreach (var field in fields)
                {
                    if (node.Attributes[field.Name] != null)
                    {
                        //_field.SetValue(item, _node.Attributes[_field.Name].Value);

                        if (node.Attributes[field.Name].Value == "")
                            field.SetValue(item, null);
                        else
                        {
                            if (field.FieldType.Name == "String")
                                field.SetValue(item, node.Attributes[field.Name].Value);
                            else
                            {
                                //Find the parse method
                                MethodInfo methodInfo;
                                Type parseType;

                                //Check if this is a nullable type, if it is get the base type's parse method, otherwise get it's type's parse method
                                if (field.FieldType.Name.StartsWith("Nullable"))
                                {
                                    methodInfo = field.FieldType.GetProperties()[1].PropertyType.GetMethod("Parse", new[] { typeof(String) });
                                    parseType = field.FieldType.GetProperties()[1].PropertyType;
                                }
                                else
                                {
                                    methodInfo = field.FieldType.GetMethod("Parse", new[] { typeof(String) });
                                    parseType = field.FieldType;
                                }

                                //Make sure we have a method to call
                                if (methodInfo != null)
                                {
                                    //Try Parse the thing
                                    field.SetValue(item, methodInfo.Invoke(parseType, new object[] { node.Attributes[field.Name].Value }));
                                }
                                else
                                    throw new Exception("There was an error parsing the data.");
                            }
                        }
                    }
                }
                retVal.Add(item);
            }

            return retVal;
        }

        public static IEnumerable<string> GetDynamicMembersFromJson(string json)
        {
            var o = JObject.Parse(json);

            var retVal = new List<string>();

            foreach (var j in o)
            {
                retVal.Add(j.Key);
            }
            
            return retVal;
        }

        public static IEnumerable<string> GetDeepDynamicMembersFromJson(string json, bool? mustAppend = null, List<string> retVal = null)
        {
            var o = JObject.Parse(json);
            if (retVal == null)
            {
                retVal = new List<string>();    
            }

            dynamic data = Json.Decode(json);

            

            dynamic stuff = JsonConvert.DeserializeObject(json);

            foreach (var j in o)
            {
                if (mustAppend.HasValue && mustAppend.Value)
                {
                    retVal[retVal.Count - 1] = retVal[retVal.Count - 1] + "[" + j.Key + "]";
                }
                else
                {
                    retVal.Add("[" + j.Key + "]");    
                }
                
                if (j.Value.HasValues)
                {
                    GetDeepDynamicMembersFromJson(j.Value.ToString(),true, retVal);
                }
                else
                {
                    mustAppend = false;
                }
            }

            return retVal;
        }

        public static String createHeader(String str)
        {
            Regex re = new Regex("[;\\\\/:*?\"<>|&'-]");
            str = re.Replace(str, " ");
            str = UppercaseWords(str);
            str = str.Trim();
            str = AddSpacesToSentence(str, true);

            return str;
        }

        static string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }

        static string AddSpacesToSentence(string text, bool preserveAcronyms)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]))
                    if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                        (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                         i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                        newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }
        #endregion Serialization

        #region Logging

        public static void LogError(Exception ex)
        {

            try
            {
                if (!EventLog.SourceExists(ConfigurationManager.AppSettings["LogSource"]))
                    EventLog.CreateEventSource(ConfigurationManager.AppSettings["LogSource"], ConfigurationManager.AppSettings["LogName"]);

                byte[] data;

                if (ex.StackTrace != null)
                    data = Encoding.ASCII.GetBytes(ex.StackTrace);
                else
                    data = new byte[] { };

                var eventLog = new EventLog();
                eventLog.Source = ConfigurationManager.AppSettings["LogSource"];
                eventLog.WriteEntry((string.IsNullOrEmpty(ex.Source) ? "" : ex.Source) + "\r\n" + ex.Message, EventLogEntryType.Error, 0, 0, data);
                eventLog.Close();

                if (ex.InnerException != null)
                    LogError(ex.InnerException);
            }
            catch
            {
            }
        }

        /// <summary>
        /// This method logs item into the event viewer.
        /// </summary>
        /// <param name="message">This is the message to log</param>
        /// <param name="stackTrace">This is additional data for error, it is the stack trace.</param>
        /// <param name="eventLogEntryType">This is the type of entry to make, Information, Error, Warning etc.</param>
        //[DebuggerStepThrough]
        public static void LogItem(String message, String stackTrace, EventLogEntryType eventLogEntryType)
        {
            var logLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);
            var mustLog = false;

            if (logLevel == 1)
                return;

            switch (eventLogEntryType)
            {
                case EventLogEntryType.Error:
                    mustLog = logLevel >= 2;
                    break;

                case EventLogEntryType.Warning:
                    mustLog = logLevel >= 3;
                    break;

                case EventLogEntryType.SuccessAudit:
                    mustLog = logLevel >= 4;
                    break;

                case EventLogEntryType.FailureAudit:
                    mustLog = logLevel >= 4;
                    break;

                case EventLogEntryType.Information:
                    mustLog = logLevel >= 5;
                    break;
            }

            if (mustLog)
            {
                if (!EventLog.SourceExists(ConfigurationManager.AppSettings["LogSource"]))
                    EventLog.CreateEventSource(ConfigurationManager.AppSettings["LogSource"], ConfigurationManager.AppSettings["LogName"]);

                var eventLog = new EventLog();
                eventLog.Source = ConfigurationManager.AppSettings["LogSource"];

                if (stackTrace == "")
                    eventLog.WriteEntry(message, eventLogEntryType);
                else
                    eventLog.WriteEntry(message, eventLogEntryType, 0, 0, Encoding.ASCII.GetBytes(stackTrace));

                eventLog.Close();

            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// This method formats the return messages for the webservice.
        /// </summary>
        /// <param name="statusCode">This is the code to insert. 0 Is always success, anything else is an error</param>
        /// <param name="statusMessage">This is the message to include</param>
        /// <returns>String: The formatted message.</returns>
        [DebuggerStepThrough]
        public static String FormatMessage(String statusCode, String statusMessage)
        {
            String data;
            data = "<Result>";
            data += "\n\t";
            data += "<StatusCode>" + statusCode + "</StatusCode>";
            data += "\n\t";
            data += "<StatusMessage>" + statusMessage + "</StatusMessage>";
            data += "\n\t";
            data += "<StatusData></StatusData>";
            data += "\n";
            data += "</Result>";

            return data;
        }

        /// <summary>
        /// This method formats the return messages for the webservice.
        /// </summary>
        /// <param name="statusCode">This is the code to insert. 0 Is always success, anything else is an error</param>
        /// <param name="statusMessage">This is the message to include</param>
        /// <param name="statusData">This is additional data to send back.</param>
        /// <returns>String: The formatted message.</returns>
        [DebuggerStepThrough]
        public static String FormatMessage(String statusCode, String statusMessage, String statusData)
        {
            String data;
            data = "<Result>";
            data += "\n\t";
            data += "<StatusCode>" + statusCode + "</StatusCode>";
            data += "\n\t";
            data += "<StatusMessage>" + statusMessage + "</StatusMessage>";
            data += "\n\t";
            data += "<StatusData>" + statusData + "</StatusData>";
            data += "\n";
            data += "</Result>";

            return data;
        }

        /// <summary>
        /// This method formats the return messages for the webservice.
        /// </summary>
        /// <param name="statusCode">This is the code to insert. 0 Is always success, anything else is an error</param>
        /// <param name="statusMessage">This is the message to include</param>
        /// <param name="statusData">This is additional data to send back.</param>
        /// <returns>String: The formatted message.</returns>
        [DebuggerStepThrough]
        public static String FormatMessage(String statusCode, String statusMessage, List<String> statusData)
        {
            String data;
            data = "<Result>";
            data += "\n\t";
            data += "<StatusCode>" + statusCode + "</StatusCode>";
            data += "\n\t";
            data += "<StatusMessage>" + statusMessage + "</StatusMessage>";
            data += "\n\t";
            data += "<StatusData>";

            foreach (var item in statusData)
            {
                data += "<Item>" + item + "</Item>";
            }

            data += "</StatusData>";
            data += "\n";
            data += "</Result>";

            return data;
        }

        /// <summary>
        /// Hack, for verion 1 to be refactored!
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static String CleanXmlString(String xml)
        {
            var data = xml.Replace("~", "&quot;").Replace("&", "&amp;");

            return data;
        }

        /// <summary>
        /// Returns a MD5 hash as a string
        /// </summary>
        /// <param name="textToHash">String to be hashed.</param>
        /// <returns>Hash as string.</returns>
        public static byte[] GetMd5Hash(String textToHash)
        {
            //Check wether data was passed
            if (string.IsNullOrEmpty(textToHash))
            {
                return null;
            }

            //Calculate MD5 hash. This requires that the string is splitted into a byte[].
            MD5 md5 = new MD5CryptoServiceProvider();
            var hash = Encoding.Default.GetBytes(textToHash);
            var result = md5.ComputeHash(hash);

            //Convert result back to string.
            return result;
        }

       /// <summary>
        /// Removes white space from json string, property values stay the same
       /// </summary>
       /// <param name="jsonString">String to be cleaned</param>
       /// <param name="isNested">Nested json property</param>
       /// <returns>Clean string</returns>
        public static string CleanNestedJsonString(string jsonString, bool isNested)
        {
            if (isNested)
            {
                jsonString = jsonString.TrimStart(Convert.ToChar("[")).TrimEnd(Convert.ToChar("]"));
            }
            //
            return jsonString;
            return Json.Encode(Json.Decode(jsonString));
        }

        const string HTML_TAG_PATTERN = "<.*?>";

        /// <summary>
        /// REmoves html tags from string
        /// </summary>
        /// <param name="textToClean">String to be Cleaned</param>
        /// <returns></returns>
        public static string StripHTML(string inputString)
        {
            return Regex.Replace
              (inputString, HTML_TAG_PATTERN, string.Empty);
        }

        #endregion

        #region Security

        /// <summary>
        /// This will hash the data parameter and return an SHA256 hash
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetHashOfString(string data)
        {
            byte[] hashedDataBytes;
            var encoder = new UTF8Encoding();

            var hasher = new SHA256Managed();
            hashedDataBytes = hasher.ComputeHash(encoder.GetBytes(data));

            return BitConverter.ToString(hashedDataBytes).ToLower().Replace("-", "");
        }

        public const string HashInstance = "";

        

        #endregion

        #region Validators

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        #endregion Validators

    }
}
