﻿using System;
using System.Configuration;

namespace NReality.MyTwitter.Helpers
{
    public class GlobalSettings
    {

        #region Singleton

        private static volatile GlobalSettings _instance;

        private static readonly object SyncRoot = new object();

        /// <summary>
        /// Read only -> returns a running instance
        /// </summary>
        public static GlobalSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    //Avoid dead locks by locking an object other than the type itself
                    lock (SyncRoot)
                    {
                        //double check the state of the instance
                        if (_instance == null)
                            _instance = new GlobalSettings();
                    }
                }

                //return the running instance
                return _instance;
            }
        }

        #endregion Singleton

        public string Hash => "HashInstance";

        public int BatchSize
        {
            get { return int.Parse(ConfigurationManager.AppSettings["batchSize"]); }
            set
            {
                Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                configuration.AppSettings.Settings["batchSize"].Value = value.ToString();
                configuration.Save();
                
            }
        }

        public int ThreadEnterTimeout => Int32.Parse(ConfigurationManager.AppSettings["ThreadEnterTimeout"]);

        public double SessionDuration => Int32.Parse(ConfigurationManager.AppSettings["SessionDuration"]);

        public bool DbMustLog => Boolean.Parse(ConfigurationManager.AppSettings["DbMustLog"]);

        public string TwitterApi => ConfigurationManager.AppSettings["TwitterApi"];
        public string TwitterConsumerKey => ConfigurationManager.AppSettings["TwitterConsumerKey"];
        public string TwitterConsumerSecret => ConfigurationManager.AppSettings["TwitterConsumerSecret"];

    }
}
