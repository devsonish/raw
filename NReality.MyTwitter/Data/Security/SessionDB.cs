﻿using System;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using NReality.MyTwitter.Artefacts.Security;
using NReality.MyTwitter.Helpers;

namespace NReality.MyTwitter.Data.Security
{
    /// <summary>
    /// Sample of data access class...
    /// </summary>
    internal static partial class SessionDB
    {
        #region Session Stored Proc Names, Parameters...

        private struct StoredProc
        {
            public const string Session_GetById = "Security.Session_GetById";
            public const string Session_Search = "Security.Session_Search";
            public const string Session_Create = "Security.Session_Create";
            public const string Session_Update = "Security.Session_Update";
            public const string Session_Delete = "Security.Session_Delete";
        }

        private struct Parameters
        {
            public const string MustLog = "@MustLog";
            public const string SessionId = "@SessionId";
            public const string UserId = "@UserId";
            public const string SessionKey = "@SessionKey";
            public const string SessionStart = "@SessionStart";
            public const string ModifiedDate = "@ModifiedDate";
            public const string ModifiedUserId = "@ModifiedUserId";
            public const string IsActive = "@IsActive";

        }

        #endregion Session Stored Proc Names, Parameters...

        #region GetById Session

        /// <summary>
        /// GetById Session... 
        /// </summary>
        /// <param name="sessionId">int</param>
        /// <returns>Session</returns>
        internal static Session GetById(int sessionId)
        {
            //Return object...
            Session retVal = null;

            try
            {
                //Create database...
                var database = DatabaseFactory.CreateDatabase(Data.Database);

                //Create command...
                using (var cmd = database.GetStoredProcCommand(StoredProc.Session_GetById))
                {
                    //Set parameter...
                    database.AddInParameter(cmd, Parameters.SessionId, DbType.Int32, sessionId);

                    //Execute...
                    using (IDataReader reader = database.ExecuteReader(cmd))
                    {
                        if (reader != null && reader.Read())
                        {
                            retVal = new Session(reader);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retVal;
        }

        #endregion GetById session

        #region Search Session

        /// <summary>
        /// Search Session... 
        /// </summary>
        /// <param name="userId">Int32</param>
        /// <param name="sessionKey">Guid</param>
        /// <param name="sessionStart">DateTime</param>
        /// <param name="modifiedDate">DateTime</param>
        /// <param name="modifiedUserId">Int32</param>
        /// <param name="isActive">Boolean</param>
        /// <returns>List</returns>
        public static Sessions Search(int? userId = null,
                                Guid? sessionKey = null,
                                DateTime? sessionStart = null,
                                DateTime? modifiedDate = null,
                                int? modifiedUserId = null,
                                bool? isActive = null)
        {
            //Return object...
            var retVal = new Sessions();

            try
            {
                //Create database...
                var database = DatabaseFactory.CreateDatabase(Data.Database);

                //Create command...
                using (var cmd = database.GetStoredProcCommand(StoredProc.Session_Search))
                {
                    //Set parameters...
                    database.AddInParameter(cmd, Parameters.UserId, DbType.Int32, userId);
                    database.AddInParameter(cmd, Parameters.SessionKey, DbType.Guid, sessionKey);
                    database.AddInParameter(cmd, Parameters.SessionStart, DbType.DateTime, sessionStart);
                    database.AddInParameter(cmd, Parameters.ModifiedDate, DbType.DateTime, modifiedDate);
                    database.AddInParameter(cmd, Parameters.ModifiedUserId, DbType.Int32, modifiedUserId);
                    database.AddInParameter(cmd, Parameters.IsActive, DbType.Boolean, isActive);

                    //Execute...
                    using (IDataReader reader = database.ExecuteReader(cmd))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                retVal.Add(new Session(reader));
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retVal;
        }

        #endregion Search Session

        #region Update Session

        /// <summary>
        /// Update Session row... 
        /// </summary>
        /// <param name="session">DataObject.Session</param>
        internal static void Update(Session session)
        {
            try
            {
                //Create database...
                var database = DatabaseFactory.CreateDatabase(Data.Database);

                using (var cmd = database.GetStoredProcCommand(StoredProc.Session_Update))
                {
                    //input parameters
                    database.AddInParameter(cmd, Parameters.MustLog, DbType.Boolean, GlobalSettings.Instance.DbMustLog);
                    database.AddInParameter(cmd, Parameters.SessionId, DbType.Int32, session.SessionId);
                    database.AddInParameter(cmd, Parameters.UserId, DbType.Int32, session.UserId);
                    database.AddInParameter(cmd, Parameters.SessionKey, DbType.Guid, session.SessionKey);
                    database.AddInParameter(cmd, Parameters.SessionStart, DbType.DateTime, session.SessionStart);
                    database.AddInParameter(cmd, Parameters.IsActive, DbType.Boolean, session.IsActive);
                    database.AddInParameter(cmd, Parameters.ModifiedUserId, DbType.Int32, session.ModifiedUserId);
                    //execute update...
                    database.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Update Session

        #region Delete Session

        /// <summary>
        /// Delete Session row... 
        /// </summary>
        /// <param name="sessionId">Int32</param>
        /// <param name="modifiedUserId">modified user id</param>
        internal static void Delete(Int32 sessionId, Int32 modifiedUserId)
        {
            try
            {
                //Create database...
                var database = DatabaseFactory.CreateDatabase(Data.Database);

                using (var cmd = database.GetStoredProcCommand(StoredProc.Session_Delete))
                {
                    //Input parameters...
                    database.AddInParameter(cmd, Parameters.MustLog, DbType.Boolean, GlobalSettings.Instance.DbMustLog);
                    database.AddInParameter(cmd, Parameters.SessionId, DbType.Int32, sessionId);
                    database.AddInParameter(cmd, Parameters.ModifiedUserId, DbType.Int32, modifiedUserId);

                    //Execute delete...
                    database.ExecuteNonQuery(cmd);

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Delete Session
    }

    /// 
    /// Custom SQL, exclude generated code
    /// 
    internal static partial class SessionDB
    {
        #region Session Custom Stored Proc Names, Parameters...

        private struct StoredProcCustom
        {
            public const string Session_DeleteByUserId = "Security.Session_DeleteByUserId";
        }

        private struct ParametersCustom
        {
        }

        #endregion Session Custom Stored Proc Names, Parameters...

        #region Create Session

        /// <summary>
        /// Create Session row and return primary key... 
        /// </summary>
        /// <param name="session">Artefact.Session</param>
        /// <returns>int?</returns>
        internal static int? Create(SessionNew session)
        {
            //Return object...
            int? sessionId = null;

            try
            {
                //Create database...
                var database = DatabaseFactory.CreateDatabase(Data.Database);

                using (var cmd = database.GetStoredProcCommand(StoredProc.Session_Create))
                {
                    //output parameter (id)
                    database.AddOutParameter(cmd, Parameters.SessionId, DbType.Int32, 4);
                    //input parameters
                    database.AddInParameter(cmd, Parameters.MustLog, DbType.Boolean, false); //Never log session creation...
                    database.AddInParameter(cmd, Parameters.UserId, DbType.Int32, session.UserId);
                    database.AddInParameter(cmd, Parameters.SessionKey, DbType.Guid, session.SessionKey);
                    database.AddInParameter(cmd, Parameters.SessionStart, DbType.DateTime, session.SessionStart);
                    database.AddInParameter(cmd, Parameters.IsActive, DbType.Boolean, session.IsActive);
                    database.AddInParameter(cmd, Parameters.ModifiedUserId, DbType.Int32, session.ModifiedUserId);

                    database.ExecuteNonQuery(cmd);

                    //Set the primary key...
                    sessionId = Int32.Parse(database.GetParameterValue(cmd, Parameters.SessionId).ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }

            return sessionId;
        }

        #endregion Create Session

        #region Delete Session By UserId

        /// <summary>
        /// Delete all sessions for a user
        /// </summary>
        /// <param name="userId">Int32</param>
        internal static void DeleteByUserId(Int32 userId)
        {
            try
            {
                //Create database...
                var database = DatabaseFactory.CreateDatabase(Data.Database);

                using (var cmd = database.GetStoredProcCommand(StoredProcCustom.Session_DeleteByUserId))
                {
                    //Input parameters...
                    database.AddInParameter(cmd, Parameters.UserId, DbType.Int32, userId);

                    //Execute delete...
                    database.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Delete Session
    }
}
