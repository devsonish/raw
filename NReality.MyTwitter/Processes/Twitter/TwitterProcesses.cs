﻿using System;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using NReality.MyTwitter.Artefacts.Common;
using NReality.MyTwitter.Artefacts.Twitter;
using NReality.MyTwitter.Cache.Twitter;
using NReality.MyTwitter.Exceptions;
using NReality.MyTwitter.Helpers;

namespace NReality.MyTwitter.Processes.Twitter
{
    public class TwitterProcesses
    {
        /// <summary>
        /// Save a tweet to memory
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="description">Save description</param>
        /// <param name="saveValue"></param>
        /// <returns>CallReturn</returns>
        public static CallReturn SaveTweet(int userId, JValue description, JToken saveValue)
        {
            var retVal = new CallReturn();

            try
            {
                //Validate no desctiption...
                if (description == null)
                    throw new ArgumentNullException(nameof(description));

                //Validate unique file name...
                if (SaveTweetCache.Instance.Exists(a => string.Equals(a.Description.Value.ToString().Trim(),
                                                            description.Value.ToString().Trim(),
                                                            StringComparison.CurrentCultureIgnoreCase)
                                                        && a.UserId == userId))
                    throw new TweetSaveException("The name '" + description + "' already exists, please specify another description.");

                //Generate new guid for id...
                var id = Guid.NewGuid();

                //New tweet....
                var saveTweet = new SaveTweet
                {
                    Id = id,
                    UserId = userId,
                    Description = description,
                    SaveValue = saveValue
                };

                //Add to cache...
                SaveTweetCache.Instance.Add(saveTweet);

                //Set id...
                retVal.Object = id;
            }
            catch (TweetSaveException tex)
            {
                retVal.SetError(ErrorType.SaveTweet_Failure, tex);
            }
            catch (Exception ex)
            {
                GeneralHelper.LogItem(ex.Message, ex.StackTrace, EventLogEntryType.Error);
                retVal.SetError(ErrorType.SystemError, ex);
            }

            // Complete and return...
            return retVal;
        }

        /// <summary>
        /// Gets a list of save tweets from memory
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <returns>CallReturn</returns>
        public static CallReturn GetSavedTweets(int userId)
        {
            var retVal = new CallReturn();

            try
            {
                retVal.Object = SaveTweetCache.Instance.FindAll(a => a.UserId == userId).ConvertAll(a => new SaveTweetItem
                {
                    Id = a.Id,
                    Description = a.Description
                });
            }
            catch (TweetSaveException tex)
            {
                retVal.SetError(ErrorType.SaveTweet_Failure, tex);
            }
            catch (Exception ex)
            {
                GeneralHelper.LogItem(ex.Message, ex.StackTrace, EventLogEntryType.Error);
                retVal.SetError(ErrorType.SystemError, ex);
            }

            // Complete and return...
            return retVal;
        }

        /// <summary>
        /// Get a single saved item
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>CallReturn</returns>
        public static CallReturn GetSavedTweet(Guid id)
        {
            var retVal = new CallReturn();

            try
            {
                retVal.Object = SaveTweetCache.Instance.Find(a => a.Id == id).SaveValue;
            }
            catch (TweetSaveException tex)
            {
                retVal.SetError(ErrorType.SaveTweet_Failure, tex);
            }
            catch (Exception ex)
            {
                GeneralHelper.LogItem(ex.Message, ex.StackTrace, EventLogEntryType.Error);
                retVal.SetError(ErrorType.SystemError, ex);
            }

            // Complete and return...
            return retVal;
        }


        /// <summary>
        /// Clear saves for a user
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <returns>CallReturn</returns>
        public static CallReturn ClearSavedTweets(int userId)
        {
            var retVal = new CallReturn();

            try
            {
                retVal.Object = SaveTweetCache.Instance.RemoveAll(a => a.UserId == userId);
            }
            catch (TweetSaveException tex)
            {
                retVal.SetError(ErrorType.SaveTweet_Failure, tex);
            }
            catch (Exception ex)
            {
                GeneralHelper.LogItem(ex.Message, ex.StackTrace, EventLogEntryType.Error);
                retVal.SetError(ErrorType.SystemError, ex);
            }

            // Complete and return...
            return retVal;
        }
    }
}