﻿using System;
using System.Diagnostics;
using System.Net;
using NReality.MyTwitter.Artefacts.Common;
using NReality.MyTwitter.Exceptions;
using NReality.MyTwitter.Helpers;
using RestSharp;
using RestSharp.Authenticators;

namespace NReality.MyTwitter.Processes.Twitter
{
    /// <summary>
    /// Manage calls to the tTwitter API
    /// </summary>
    public static class ApiProcesses
    {
        #region Public methods

        /// <summary>
        ///  Twitter search
        /// </summary>
        /// <param name="searchTerm">Term to search for</param>
        /// <returns>CallReturn</returns>
        public static CallReturn SearchTweets(string searchTerm)
        {
            var retVal = new CallReturn();

            try
            {
                retVal = SearchTwitter(searchTerm);
            }
            catch (TwitterAuthorisaionException tex)
            {
                retVal.SetError(ErrorType.Authorisation_Denied, tex);
            }
            catch (Exception ex)
            {
                GeneralHelper.LogItem(ex.Message, ex.StackTrace, EventLogEntryType.Error);
                retVal.SetError(ErrorType.SystemError, ex);
            }

            // Complete and return...
            return retVal;
        }

        /// <summary>
        /// Twitter search by handle
        /// </summary>
        /// <param name="twitterHandle">Twitter handle to search against</param>
        /// <returns>CallReturn</returns>
        public static CallReturn SearchTweetsByTwitterHandle(string twitterHandle)
        {
            var retVal = new CallReturn();

            try
            {
                retVal = SearchTwitter("from:" + twitterHandle);
            }
            catch (TwitterAuthorisaionException tex)
            {
                retVal.SetError(ErrorType.Authorisation_Denied, tex);
            }
            catch (Exception ex)
            {
                GeneralHelper.LogItem(ex.Message, ex.StackTrace, EventLogEntryType.Error);
                retVal.SetError(ErrorType.SystemError, ex);
            }

            // Complete and return...
            return retVal;
        }

        #endregion Public methods   

        #region Private methods

        /// <summary>
        ///  General twitter search method
        /// </summary>
        /// <param name="searchTerm">What to search for</param>
        /// <returns>CallReturn</returns>
        private static CallReturn SearchTwitter(string searchTerm)
        {
            var retVal = new CallReturn();

            //New twitter rest client...
            var client = new RestClient(GlobalSettings.Instance.TwitterApi)
            {
                Authenticator = OAuth1Authenticator.ForRequestToken(GlobalSettings.Instance.TwitterConsumerKey,
                    GlobalSettings.Instance.TwitterConsumerSecret)
            };

            //New twitter search request...
            var request = new RestRequest("/1.1/search/tweets.json", Method.GET);
            request.AddParameter("q", searchTerm);
            var response = client.Execute(request);

            //Manage unauthorized...
            if (response.StatusCode == HttpStatusCode.Unauthorized)
                throw new TwitterAuthorisaionException();

            //Set response...
            retVal.Object = response.Content;

            // Complete and return...
            return retVal;
        }

        #endregion Private methods
    }
}