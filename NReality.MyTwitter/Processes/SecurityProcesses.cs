﻿using System;
using System.Diagnostics;
using System.Security.Authentication;
using NReality.MyTwitter.Artefacts.Common;
using NReality.MyTwitter.Artefacts.Security;
using NReality.MyTwitter.Cache.Security;
using NReality.MyTwitter.Helpers;

namespace NReality.MyTwitter.Processes
{
    public static class SecurityProcesses
    {
        #region Public methods

        #region Session

        /// <summary>
        ///     <p>Gets all active sessions</p>
        /// </summary>
        public static CallReturn GetActiveSessions()
        {
            var retVal = new CallReturn();

            try
            {
                //Access datalayer here, manage sessions, return sessions...
                retVal.Object = new Sessions();
            }
            catch (Exception ex)
            {
                GeneralHelper.LogItem(ex.Message, ex.StackTrace, EventLogEntryType.Error);
                retVal.SetError(ErrorType.SystemError, ex);
            }

            // Complete and return the state
            return retVal;
        }

        /// <summary>
        ///     Creates a new session for a user
        /// </summary>
        /// <param name="userId">User Id</param>
        public static CallReturn CreateSession(int userId)
        {
            var retVal = new CallReturn();

            try
            {
                //New session...
                var session = new SessionNew
                {
                    SessionKey = Guid.NewGuid(),
                    SessionStart = DateTime.Now,
                    UserId = userId,
                    IsActive = true,
                    ModifiedUserId = (int) User.Values.Admin
                };

                //Save to db and set id...

                //Set return object...
                retVal.Object = session;
            }
            catch (Exception ex)
            {
                GeneralHelper.LogItem(ex.Message, ex.StackTrace, EventLogEntryType.Error);
                retVal.SetError(ErrorType.SystemError, ex);
            }

            return retVal;
        }

        #endregion Session

        #region Logon

        /// <summary>
        /// Logon
        /// </summary>
        /// <param name="userName">UserName</param>
        /// <param name="password">Password</param>
        /// <param name="isPasswordHashed">Flag</param>
        /// <returns>CallReturn</returns>
        public static CallReturn Logon(string userName, string password, bool isPasswordHashed)
        {
            var retVal = new CallReturn();

            try
            {
                //Validate...
                if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                    throw new ArgumentNullException(string.IsNullOrEmpty(userName) ? "userName" : "password");

                //Find user by name...
                var user = UserCache.Instance.Find(a => a.UserName == userName);

                if (user == null || user.UserId == 0)
                    throw new AuthenticationException();


                //Compare password...
                if (isPasswordHashed ? PasswordHelpers.ValidatePassword(user.Password, password) : user.Password.Equals(password))
                    retVal.Object = new LogonDetail
                    {
                        Password = user.Password,
                        UserId = user.UserId,
                        UserName = user.UserName
                    };
                else
                    throw new AuthenticationException();
            }
            catch (AuthenticationException)
            {
                retVal.SetError(ErrorType.Authorisation_Denied);
                return retVal;
            }
            catch (Exception ex)
            {
                retVal.SetError(ErrorType.UNKNOWN, ex);
                return retVal;
            }

            return retVal;
        }

        #endregion Logon

        #endregion Public methods
    }
}