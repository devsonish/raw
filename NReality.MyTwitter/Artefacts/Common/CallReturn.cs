﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using NReality.MyTwitter.Exceptions;
using NReality.MyTwitter.Helpers;

namespace NReality.MyTwitter.Artefacts.Common
{
    public class CallReturn
    {
        #region Constructors

        /// <summary>
        /// The default constructor for the call return.  Sets an empty error list and defaults the State to Success.
        /// </summary>
        public CallReturn()
        {
            Errors = new ErrorList();
            Object = null;
            State = CallReturnState.Success;
        }

        #endregion

        #region Properties

        #region Attributes

        /// <summary>
        /// Contains the state of the call.
        /// </summary>
        [DataMember]
        public CallReturnState State;

        #endregion

        #region Elements

        /// <summary>
        /// Contains a list of errors.
        /// </summary>
        [DataMember]
        public ErrorList Errors;

        /// <summary>
        /// Contains a list of validation errors
        /// </summary>
        [DataMember]
        public ValidationMessages ValidationMessages;

        /// <summary>
        /// Contains the returned object.
        /// </summary>
        [DataMember]
        public object Object;

        /// <summary>
        /// The exception
        /// </summary>
        [DataMember]
        public Exception Exception;

        #endregion

        #endregion

        #region Methods

        internal void SetValidationError(string message)
        {
            State = CallReturnState.ValidationError;

            var validationMessage = new ValidationMessage
            {
                Message = message
            };

            if (!ValidationMessages.Contains(validationMessage))
            {
                ValidationMessages.Add(validationMessage);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorType"></param>
        /// <param name="ex"></param>
        internal void SetError(ErrorType errorType, Exception ex)
        {
            State = CallReturnState.Failure;

            Exception = ex;
            Error item = new Error((int)errorType, ex.Message);

            if (!Errors.Contains(item))
            {
                Errors.Add(item);

                //Not applicable in prod
                //if (ex.InnerException != null)
                //	SetError(errorType, ex.InnerException);
            }
        }

        internal void SetError(ErrorType errorType, AuthorisationException ex)
        {
            State = CallReturnState.Failure;

            Error item = new Error((int)errorType, ErrorMessageHelper.GetErrorText(errorType, new object[] { }));

            if (!Errors.Contains(item))
            {
                Errors.Add(item);
            }
        }

        internal void SetError(ErrorType errorType, params object[] args)
        {
            State = CallReturnState.Failure;

            Error item = new Error((int)errorType, ErrorMessageHelper.GetErrorText(errorType, args));

            if (!Errors.Contains(item))
                Errors.Add(item);
        }

        #region SetWarning

        /// <summary>
        /// 
        /// </summary>
        /// <param name="warningType"></param>
        /// <param name="ex"></param>
        internal void SetWarning(WarningType warningType, Exception ex)
        {
            if (State != CallReturnState.Failure)
                State = CallReturnState.Warning;

            Error item = new Error((int)warningType, ex.Message);

            if (!Errors.Contains(item))
                Errors.Add(item);
        }

        internal void SetWarning(WarningType warningType, params object[] args)
        {
            if (State != CallReturnState.Failure)
                State = CallReturnState.Warning;

            Error item = new Error((int)warningType, ErrorMessageHelper.GetWarningText(warningType, args));

            if (!Errors.Contains(item))
                Errors.Add(item);
        }

        #endregion

        #region Deserialize

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal string Serialize()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CallReturn));
                using (StringWriter sw = new StringWriter())
                {
                    serializer.Serialize(sw, this);
                    return sw.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "\\");
                }
            }
            catch (Exception ex)
            {
                CallReturn error = new CallReturn();
                error.SetError(ErrorType.Serialization_Faillure, ex);
                return error.Serialize();
            }
        }

        #endregion

        #endregion

        #region operators

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static CallReturn operator +(CallReturn lhs, CallReturn rhs)
        {
            CallReturn retVal = new CallReturn();

            if (lhs.State == CallReturnState.Failure || rhs.State == CallReturnState.Failure)
            {
                retVal.State = CallReturnState.Failure;
            }
            else if (lhs.State == CallReturnState.Warning || rhs.State == CallReturnState.Warning)
            {
                retVal.State = CallReturnState.Warning;
            }
            else
            {
                retVal.State = CallReturnState.Success;
            }

            foreach (Error item in lhs.Errors)
            {
                if (!retVal.Errors.Contains(item))
                    retVal.Errors.Add(item);
            }
            foreach (Error item in rhs.Errors)
            {
                if (!retVal.Errors.Contains(item))
                    retVal.Errors.Add(item);
            }

            retVal.Object = lhs.Object;

            return retVal;
        }

        #endregion

        //public IEnumerator GetEnumerator()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
