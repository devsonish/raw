﻿using System;
using System.Runtime.Serialization;

namespace NReality.MyTwitter.Artefacts.Common
{
    [Serializable]
    public abstract class Artefact 
    {

        [DataMember]
        public DateTime ModifiedDate
        {
            get;
            set;
        }

        [DataMember]
        public Int32 ModifiedUserId
        {
            get;
            set;
        }

        [DataMember]
        public Boolean IsActive
        {
            get;
            set;
        }
    }
}
