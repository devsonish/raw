﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NReality.MyTwitter.Artefacts.Common
{
    [CollectionDataContract]
    public class ErrorList : List<Error>
    {
    }
}
