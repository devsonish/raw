﻿using System;
using System.Data;
using System.Runtime.Serialization;
using NReality.MyTwitter.Artefacts.Common;

namespace NReality.MyTwitter.Artefacts.Security
{
    [DataContract]
    public class Session : Artefact
    {
        #region Fields

        private enum Fields
        {
            SessionId,
            UserId,
            SessionKey,
            SessionStart,
            ModifiedDate,
            ModifiedUserId,
            IsActive,
        }

        #endregion Fields

        #region Constructors

        public Session() { }

        public Session(IDataReader reader) 
        { 
            if(reader != null)
            {
                this.SessionId = reader.GetInt32((int)Fields.SessionId);
                this.UserId = reader.GetInt32((int)Fields.UserId);
                this.SessionKey = reader.GetGuid((int)Fields.SessionKey);
                this.SessionStart = reader.GetDateTime((int)Fields.SessionStart);
                this.ModifiedDate = reader.GetDateTime((int)Fields.ModifiedDate);
                this.ModifiedUserId = reader.GetInt32((int)Fields.ModifiedUserId);
                this.IsActive = reader.GetBoolean((int)Fields.IsActive);
            }
        }

        #endregion Constructors

        #region Public properties

        [DataMember]
        public Int32 SessionId
        {
            get;
            set;
        }

        [DataMember]
        public Int32 UserId
        {
            get;
            set;
        }

        [DataMember]
        public Guid SessionKey
        {
            get;
            set;
        }

        [DataMember]
        public DateTime SessionStart
        {
            get;
            set;
        }

        #endregion Public properties
    }
}