﻿using System;
using System.Runtime.Serialization;
using NReality.MyTwitter.Artefacts.Common;

namespace NReality.MyTwitter.Artefacts.Security
{
    [DataContract]
    public class SessionNew : Artefact
    {
        #region Fields

        private enum Fields
        {
            SessionId,
            UserId,
            SessionKey,
            SessionStart,
            ModifiedDate,
            ModifiedUserId,
            IsActive,
        }

        #endregion Fields

        #region Constructors

        public SessionNew() { }

        #endregion Constructors

        #region Public properties

        [DataMember]
        public Int32 SessionId
        {
            get;
            set;
        }

        [DataMember]
        public Int32 UserId
        {
            get;
            set;
        }

        [DataMember]
        public Guid SessionKey
        {
            get;
            set;
        }

        [DataMember]
        public DateTime SessionStart
        {
            get;
            set;
        }


        #endregion Public properties
    }
}