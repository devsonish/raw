﻿using System;
using System.Runtime.Serialization;
using NReality.MyTwitter.Artefacts.Common;

namespace NReality.MyTwitter.Artefacts.Security
{
    [DataContract]
    public class LogonCredentials : Artefact
    {
        #region Constructors

        public LogonCredentials() { }

        #endregion Constructors

        #region Public properties

        [DataMember]
        public String Email
        {
            get;
            set;
        }

        [DataMember]
        public String Password
        {
            get;
            set;
        }

        #endregion Public properties
    }
}
