﻿using System;
using System.Runtime.Serialization;
using NReality.MyTwitter.Artefacts.Common;

namespace NReality.MyTwitter.Artefacts.Security
{
    [DataContract]
    public class LogonSuccess : Artefact
    {
        #region Constructors

        public LogonSuccess() { }

        #endregion Constructors

        #region Public properties

        [DataMember]
        public Guid SessionKey;

        [DataMember]
        public Int32 UserId;

        [DataMember]
        public DateTime SessionStart;

        #endregion Public properties
    }
}
