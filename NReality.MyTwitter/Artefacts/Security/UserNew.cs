﻿using NReality.MyTwitter.Artefacts.Common;

namespace NReality.MyTwitter.Artefacts.Security
{
    internal class UserNew : Artefact
    {
        #region Public properties

        //UserId
        public int UserId { get; set; }
        //PersonId
        public int PersonId { get; set; }
        //Password
        public string Password { get; set; }

        #endregion Public properties	
    }
}