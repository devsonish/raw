﻿using NReality.MyTwitter.Artefacts.Common;

namespace NReality.MyTwitter.Artefacts.Security
{
    public class LogonDetail : Artefact
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
