﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NReality.MyTwitter.Artefacts.Security
{
    [CollectionDataContract]
    public class Users : List<User>
    {
    }
}