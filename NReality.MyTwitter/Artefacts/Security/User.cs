﻿using System.Data;
using NReality.MyTwitter.Artefacts.Common;
using NReality.MyTwitter.Attributes;

namespace NReality.MyTwitter.Artefacts.Security
{
    public partial class User
    {
        public enum Values
        {
            [StringValue("Admin")] Admin = 1
        }
    }

    public partial class User : Artefact
    {
        #region Fields

        private enum Fields
        {
            UserId,
            UserName,
            Password,
            ModifiedDate,
            ModifiedUserId,
            IsActive
        }

        #endregion Fields

        #region Constructors

        public User()
        {
        }

        public User(IDataReader reader)
        {
            if (reader != null)
            {
                UserId = reader.GetInt32((int) Fields.UserId);
                UserName = reader.GetString((int) Fields.UserName);
                Password = reader.GetString((int) Fields.Password);
                ModifiedUserId = reader.GetInt32((int) Fields.ModifiedUserId);
                IsActive = reader.GetBoolean((int) Fields.IsActive);
            }
        }

        #endregion Constructors

        #region Public properties

        //UserId
        public int UserId { get; set; }
        //PersonId
        public string UserName { get; set; }
        //Password
        public string Password { get; set; }

        #endregion Public properties	
    }
}