﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NReality.MyTwitter.Artefacts.Twitter
{
    [CollectionDataContract]
    public class SaveTweets : List<SaveTweet>
    {
    }
}