﻿using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;

namespace NReality.MyTwitter.Artefacts.Twitter
{
    public class SaveTweet : SaveTweetItem
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public JToken SaveValue { get; set; }
    }
}