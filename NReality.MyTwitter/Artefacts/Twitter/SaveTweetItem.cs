﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;

namespace NReality.MyTwitter.Artefacts.Twitter
{
    public class SaveTweetItem
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public JValue Description { get; set; }
    }
}