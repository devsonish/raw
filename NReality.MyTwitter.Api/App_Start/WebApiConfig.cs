﻿using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using NReality.MyTwitter.Api.Filters;

namespace NReality.MyTwitter.Api
{
    public static class WebApiConfig
    {
        [EnableCors("*", "*", "*", SupportsCredentials = true)]
        public static void Register(HttpConfiguration config)
        {
            // Enforce https for entire web....
            config.Filters.Add(new ForceHttpsAttribute());

            // Web API routes...
            config.MapHttpAttributeRoutes();

            //Custom Options message handler...
            //config.MessageHandlers.Add(new OptionsMessageHandler());

            // Enable CORS for all client requests...
            //config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            // That makes sure you get json on most queries, but you can get xml when you send text/xml...
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }

}
