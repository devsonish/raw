﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Http;
using NReality.MyTwitter.Api.Filters;
using NReality.MyTwitter.Api.Models.Cache;
using NReality.MyTwitter.Artefacts.Common;
using NReality.MyTwitter.Artefacts.Security;
using NReality.MyTwitter.Helpers;
using NReality.MyTwitter.Processes;

namespace NReality.MyTwitter.Api.Cache
{
    public sealed class SessionCache : IDisposable
    {
        #region private members...

        private static volatile List<SessionModel> _instance;
        private static object _syncRoot = new object();

        #endregion private members...

        public static bool Refresh()
        {
            if (_instance == null)
                return false;

            var isRefreshed = false;

            // Wait until the lock is available and lock the queue.
            try
            {
                if (Monitor.TryEnter(_instance, GlobalSettings.Instance.ThreadEnterTimeout)) //wait for thead until ready with specified timeout...
                {
                    Dispose();
                    isRefreshed = true;
                }
            }
            finally
            {
                try
                {
                    Monitor.Exit(_instance);
                }
                catch (ArgumentNullException)
                {
                    //when the instance is null it will be refreshed when read.
                    isRefreshed = true;
                }
            }

            return isRefreshed;
        }

        public static List<SessionModel> Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                        {
                            var retVal = SecurityProcesses.GetActiveSessions();

                            switch (retVal.State)
                            {
                                case CallReturnState.Failure:
                                case CallReturnState.Warning:
                                    break;

                                case CallReturnState.Success:
                                    _instance = ((Sessions) retVal.Object).ConvertAll(a => new SessionModel
                                    {
                                        Key = a.SessionKey,
                                        SessionStart = a.SessionStart,
                                        UserId = a.UserId
                                    });
                                    break;
                            }
                        }
                    }
                }

                return _instance;
            }

            set
            {
                _instance = value;
            }
        }

        public static void AddSession(SessionModel session)
        {
            if(!Instance.Contains(session))
                Instance.Add(session);
        }

        public static bool ValidateSession(Guid sessionKey)
        {
            var session = Instance.Find(a => a.Key == sessionKey);

            if (session != null && 
                session.SessionEnd != DateTime.MinValue && 
                session.SessionEnd > DateTime.Now)
            {
                if ((session.SessionEnd - DateTime.Now).Minutes < GlobalSettings.Instance.SessionDuration)
                {
                    Instance.Find(a => a.Key == sessionKey).SessionEnd = session.SessionEnd.AddMinutes(GlobalSettings.Instance.SessionDuration);   
                }

                return true;
            }
            else if (session != null)
            {
                Instance.Remove(session);
            }
            return false;
        }

        public static SessionModel GetSession(System.Net.Http.HttpRequestMessage requestMessage)
        {
            var token = requestMessage.Headers.GetValues(TokenRequiredFilter.Token).First();
            return Instance.Find(a => a.Key == Guid.Parse(token));
        }

        private static void Dispose()
        {
            GC.SuppressFinalize(_instance);
            GC.SuppressFinalize(_syncRoot);

            _instance = null;
            _syncRoot = new object();
        }

        void IDisposable.Dispose()
        {
            Dispose();
        }
    }
}