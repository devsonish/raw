﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using NReality.MyTwitter.Api.Cache;

namespace NReality.MyTwitter.Api.Filters
{
    /// <summary>
    /// Filter for token
    /// </summary>
    public class TokenRequiredFilter : ActionFilterAttribute
    {
        public const string Token = "Token";

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            if (filterContext.Request.Headers.Contains(Token))
            {
                var token = filterContext.Request.Headers.GetValues(Token).First();

                if (!SessionCache.ValidateSession(Guid.Parse(token)))
                {
                    var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Token" };
                    filterContext.Response = responseMessage;
                }
            }
            else
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}