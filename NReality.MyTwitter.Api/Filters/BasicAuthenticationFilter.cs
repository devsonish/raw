﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using NReality.MyTwitter.Artefacts.Common;
using NReality.MyTwitter.Artefacts.Security;
using NReality.MyTwitter.Helpers;
using NReality.MyTwitter.Processes;

namespace NReality.MyTwitter.Api.Filters
{
    public class BasicAuthenticationFilter : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (SkipAuthorization(actionContext)) return;

            if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
                return;

            var authHeader = actionContext.Request.Headers.Authorization;

            if (authHeader != null)
                if (authHeader.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase)
                    && !string.IsNullOrWhiteSpace(authHeader.Parameter))
                {
                    var credArray = GetCredentials(authHeader);
                    var userName = credArray[0];
                    var password = credArray[1];

                    var retVal = SecurityProcesses.Logon(userName, password, false);

                    switch (retVal.State)
                    {
                        case CallReturnState.Success:
                            var logonDetail = (LogonDetail) retVal.Object;
                            var currentPrincipal = new BasicAuthenticationIdentity(logonDetail.UserName, logonDetail.Password)
                            {
                                UserName = logonDetail.UserName,
                                Password = logonDetail.Password,
                                UserId = logonDetail.UserId
                            };
                            Thread.CurrentPrincipal = new GenericPrincipal(currentPrincipal, null);
                            return;
                        case CallReturnState.Warning:
                            break;
                        case CallReturnState.Failure:
                            if (retVal.Errors[0].ErrorNumber == (int) ErrorType.Authorisation_Denied)
                                HandleUnauthorizedRequest(actionContext);
                            else
                                actionContext.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError) {ReasonPhrase = retVal.Errors[0].Message};
                            ;
                            return;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

            HandleUnauthorizedRequest(actionContext);
        }

        /// <summary>
        ///     Manage anonymous authorisation
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            Contract.Assert(actionContext != null);

            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }

        public string[] GetCredentials(AuthenticationHeaderValue authHeader)
        {
            //Base 64 encoded string
            var rawCred = authHeader.Parameter;
            var encoding = Encoding.GetEncoding("iso-8859-1");
            var cred = encoding.GetString(Convert.FromBase64String(rawCred));

            var credArray = cred.Split(':');

            return credArray;
        }

        private bool IsResourceOwner(string userName, HttpActionContext actionContext)
        {
            var routeData = actionContext.Request.GetRouteData();
            var resourceUserName = routeData.Values["userName"] as string;

            if (resourceUserName == userName)
                return true;
            return false;
        }

        private void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);

            actionContext.Response.Headers.Add("WWW-Authenticate", "Basic Scheme='CVC'");
        }
    }
}