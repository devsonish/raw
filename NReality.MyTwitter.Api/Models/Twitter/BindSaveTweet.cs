﻿using Newtonsoft.Json.Linq;

namespace NReality.MyTwitter.Api.Models.Twitter
{
    public class BindSaveTweet
    {
        public JValue Description { get; set; }
        public JToken SaveValue { get; set; }
    }
}