﻿namespace NReality.MyTwitter.Api.Models.Twitter
{
    public class BindSearchTweets
    {
        //Default twitter search parameter...
        public string SearchTerm { get; set; }
    }

    public class BindGetTweet
    {
        //Default twitter search parameter...
        public string Id { get; set; }
    }

    public class BindSearchTweetsByHandle
    {
        //Default twitter search parameter...
        public string Handle { get; set; }
    }
}