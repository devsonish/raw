﻿namespace RIMS.CVC.Web.Models.Cache
{
    public class UserModel
    {
        public struct PropertyNames
        {
            public const string UserName = "UserName";
            public const string Password = "Password";
            public const string ClientId = "ClientId";
            public const string UserId = "UserId";
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public int ClientId { get; set; }
        public int UserId { get; set; }
    }
}