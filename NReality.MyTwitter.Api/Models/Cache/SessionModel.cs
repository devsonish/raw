﻿using System;

namespace NReality.MyTwitter.Api.Models.Cache
{
    public class SessionModel
    {
        public Int32 UserId { get; set; }
        public Int32 SessionId { get; set; }
        public Guid Key { get; set; }
        public DateTime SessionStart { get; set; }
        public DateTime SessionEnd { get; set; }
    }
}