﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NReality.MyTwitter.Api.Cache;
using NReality.MyTwitter.Api.Filters;
using NReality.MyTwitter.Api.Models.Cache;
using NReality.MyTwitter.Artefacts.Common;
using NReality.MyTwitter.Helpers;

namespace NReality.MyTwitter.Api.Controllers.Security
{
    [RoutePrefix("api/v1.0/Security/Token")]
    public class TokenController : ApiController
    {
        /// <summary>
        /// Create session and return session key
        /// </summary>
        [Route("")]
        [BasicAuthenticationFilter]
        public IHttpActionResult GetToken()
        {
            HttpResponseMessage response = null;
            
            try
            {
                //Must be authenticated...
                if (System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    //Get identity...
                    var basicAuthenticationIdentity = System.Threading.Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                    if (basicAuthenticationIdentity != null)
                    {
                        //Set values and create session...
                        var userId = basicAuthenticationIdentity.UserId;
                        var retVal = Processes.SecurityProcesses.CreateSession(userId);

                        switch (retVal.State)
                        {
                            case CallReturnState.Success:
                                var session = ((Artefacts.Security.SessionNew)retVal.Object);

                                //Add session to cache...
                                SessionCache.AddSession(new SessionModel
                                {
                                    UserId = userId,
                                    SessionId = session.SessionId,
                                    Key = session.SessionKey,
                                    SessionStart = session.SessionStart,
                                    SessionEnd = session.SessionStart.AddMinutes(GlobalSettings.Instance.SessionDuration)
                                });

                                //Set token response...
                                response = new HttpResponseMessage(HttpStatusCode.OK);
                                response.Headers.Add("Token", session.SessionKey.ToString());
                                response.Headers.Add("TokenExpiry", session.SessionStart.AddMinutes(GlobalSettings.Instance.SessionDuration).ToString());
                                response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
                                break;
                            case CallReturnState.Warning:
                                response = new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = retVal.Errors[0].Message };
                                break;
                            case CallReturnState.Failure:
                                response = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = retVal.Errors[0].Message };
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = ex.Message };
            }

            return ResponseMessage(response);
        }
    }
}