﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using NReality.MyTwitter.Artefacts.Common;

namespace NReality.MyTwitter.Api.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        public IHttpActionResult GetBadRequest(CallReturn callReturn)
        {
            if (callReturn == null)
            {
                return InternalServerError();
            }

            if (callReturn.State != CallReturnState.Success)
            {
                if (callReturn.Errors != null)
                {
                    foreach (var error in callReturn.Errors)
                    {
                        ModelState.AddModelError("", error.Message);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        public IHttpActionResult GetHttpResponseException(CallReturn callReturn)
        {

            if (callReturn.State != CallReturnState.Success)
            {
                StringBuilder errors = new StringBuilder();
                if (callReturn.Errors != null)
                {
                    foreach (var error in callReturn.Errors)
                    {
                        errors.Append(error.Message);
                    }
                    //return SystemNetHttp.HttpRequestMessageExtensions.CreateErrorResponse(null,HttpStatusCode.InternalServerError, errors.ToString());
                    return new BadRequestWithMessageResult(errors.ToString());
                }
            }



            return null;   
        }

        private class BadRequestWithMessageResult : IHttpActionResult
        {
            private readonly string message;

            public BadRequestWithMessageResult(string message)
            {
                this.message = message;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                response.Content = new StringContent(message);
                return Task.FromResult(response);
            }
        }

        [HttpOptions]
        [AllowAnonymous]
        public IHttpActionResult Options()
        {
            return Ok();
        }

    }
}