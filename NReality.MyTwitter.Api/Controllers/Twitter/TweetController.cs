﻿using System;
using System.Web.Http;
using Newtonsoft.Json;
using NReality.MyTwitter.Api.Cache;
using NReality.MyTwitter.Api.Filters;
using NReality.MyTwitter.Api.Models.Twitter;
using NReality.MyTwitter.Artefacts.Common;
using NReality.MyTwitter.Processes.Twitter;

namespace NReality.MyTwitter.Api.Controllers.Twitter
{
    [RoutePrefix("api/v1.0/Twitter/Tweet")]
    [TokenRequiredFilter]
    public class TweetController : ApiController
    {
        /// <summary>
        /// Search for tweets containing specified text
        /// </summary>
        /// <param name="model">Binding parameter model</param>
        /// <returns>CallReturn</returns>
        [HttpGet]
        [Route("Search")]
        public IHttpActionResult SearchTweets([FromUri] BindSearchTweets model)
        {
            IHttpActionResult response = null;

            try
            {
                var retVal = ApiProcesses.SearchTweets(model.SearchTerm);

                switch (retVal.State)
                {
                    case CallReturnState.Failure:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Warning:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Success:
                        response = Ok(JsonConvert.DeserializeObject(retVal.Object.ToString()));
                        break;
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return response;
        }

        /// <summary>
        /// Search for recents tweets by a particular twitter handle
        /// </summary>
        /// <param name="model">Binding parameter model</param>
        /// <returns>CallReturn</returns>
        [HttpGet]
        [Route("SearchByHandle")]
        public IHttpActionResult SearchTweetsByHandle([FromUri] BindSearchTweetsByHandle model)
        {
            IHttpActionResult response = null;

            try
            {
                var retVal = ApiProcesses.SearchTweetsByTwitterHandle(model.Handle);

                switch (retVal.State)
                {
                    case CallReturnState.Failure:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Warning:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Success:
                        response = Ok(JsonConvert.DeserializeObject(retVal.Object.ToString()));
                        break;
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return response;
        }

        /// <summary>
        /// Gets a saved tweet by a particular id
        /// </summary>
        /// <param name="model">Binding parameter model</param>
        /// <returns>CallReturn</returns>
        [HttpGet]
        [Route("{Id}")]
        public IHttpActionResult GetTweet([FromUri] BindGetTweet model)
        {
            IHttpActionResult response = null;

            try
            {
                var retVal = TwitterProcesses.GetSavedTweet(Guid.Parse(model.Id));

                switch (retVal.State)
                {
                    case CallReturnState.Failure:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Warning:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Success:
                        response = Ok(retVal.Object);
                        break;
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return response;
        }

        /// <summary>
        /// Save a search result
        /// </summary>
        /// <param name="model">Binding parameter model</param>
        /// <returns>CallReturn</returns>
        [HttpPut]
        [Route("Save")]
        public IHttpActionResult SaveTweet([FromBody] BindSaveTweet model)
        {
            IHttpActionResult response = null;

            try
            {
                var session = SessionCache.GetSession(Request);

                var retVal = TwitterProcesses.SaveTweet(session.UserId, model.Description, model.SaveValue);

                switch (retVal.State)
                {
                    case CallReturnState.Failure:
                        response = BadRequest(retVal.Exception.Message);
                        break;

                    case CallReturnState.Warning:
                        response = InternalServerError(retVal.Exception);
                        break;

                    case CallReturnState.Success:
                        response = Ok(retVal.Object);
                        break;
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return response;
        }

        /// <summary>
        /// Get saved search results
        /// </summary>
        /// <returns>CallReturn</returns>
        [HttpGet]
        [Route("GetSaved")]
        public IHttpActionResult GetSavedTweets()
        {
            IHttpActionResult response = null;

            try
            {
                var session = SessionCache.GetSession(Request);
                var retVal = TwitterProcesses.GetSavedTweets(session.UserId);

                switch (retVal.State)
                {
                    case CallReturnState.Failure:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Warning:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Success:
                        response = Ok(retVal.Object);
                        break;
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return response;
        }

        /// <summary>
        /// Clear saves from memory
        /// </summary>
        /// <returns>CallReturn</returns>
        [HttpDelete]
        [Route("ClearSaved")]
        public IHttpActionResult ClearSavedTweets()
        {
            IHttpActionResult response = null;

            try
            {
                var session = SessionCache.GetSession(Request);
                var retVal = TwitterProcesses.ClearSavedTweets(session.UserId);

                switch (retVal.State)
                {
                    case CallReturnState.Failure:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Warning:
                        response = InternalServerError();
                        break;

                    case CallReturnState.Success:
                        response = Ok();
                        break;
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return response;
        }
    }
}